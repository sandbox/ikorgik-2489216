Provides popup window with ERPAL Timetracking info:
- timetracking for current task;
- time info for today;
- time info for current week;
- time info for current month;
- statistics about working days.

Maintainer:
Sergey Korzh (ikorgik@gmail.com)
